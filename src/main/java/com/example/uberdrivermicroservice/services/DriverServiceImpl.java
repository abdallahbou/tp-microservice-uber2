package com.example.uberdrivermicroservice.services;

import com.example.uberdrivermicroservice.Exception.NotFoundException;
import com.example.uberdrivermicroservice.dto.ResponseDriverDto;
import com.example.uberdrivermicroservice.dto.request.RequestDriverDto;
import com.example.uberdrivermicroservice.entity.Driver;
import com.example.uberdrivermicroservice.repository.DriverRepository;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class DriverServiceImpl implements DriverService {

    @Autowired
    private DriverRepository _driverRepository;

    @Autowired
    private ModelMapper _modelMapper;

    @Override
    public ResponseDriverDto createDriver(RequestDriverDto passengerDTO) {

        return _modelMapper.map(_driverRepository.save(_modelMapper.map(passengerDTO, Driver.class)), ResponseDriverDto.class);
    }

    @Override
    public ResponseDriverDto getDriver(String search) throws NotFoundException {
        Driver passenger = _driverRepository.searchDriver(search);
        if (passenger == null) {
            throw new NotFoundException();
        }
        return _modelMapper.map(passenger, ResponseDriverDto.class);
    }

    @Override
    public ResponseDriverDto findDriverByEmail(String email) throws NotFoundException {

        Driver driver = _driverRepository.findDriverByEmail(email);

        if (driver == null) {
            throw new NotFoundException();
        }
        return _modelMapper.map(driver, ResponseDriverDto.class);
    }

    @Override
    public ResponseDriverDto findDriverById(int id) {
        Driver driver = _driverRepository.findById(id).get();
        if (driver == null) {
            return null;
        } else {
            return _modelMapper.map(driver, ResponseDriverDto.class);
        }


    }


}
