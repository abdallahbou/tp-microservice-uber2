package com.example.uberdrivermicroservice.services;

import com.example.uberdrivermicroservice.Exception.NotFoundException;
import com.example.uberdrivermicroservice.dto.ResponseDriverDto;
import com.example.uberdrivermicroservice.dto.request.RequestDriverDto;


public interface DriverService {

    public ResponseDriverDto createDriver(RequestDriverDto requestDriverDto) ;

    public ResponseDriverDto getDriver(String search) throws NotFoundException;


    public ResponseDriverDto findDriverByEmail(String email) throws NotFoundException;

    public ResponseDriverDto findDriverById(int id);

}
