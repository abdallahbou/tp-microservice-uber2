package com.example.uberdrivermicroservice.repository;

import com.example.uberdrivermicroservice.entity.Driver;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;


public interface DriverRepository extends CrudRepository<Driver, Integer> {


    @Query("SELECT c from Driver c where c.email like :search")
    public Driver searchDriver(@Param("search") String search);


    public Driver findDriverByEmail(String email);

}
