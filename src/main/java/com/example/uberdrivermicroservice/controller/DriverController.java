package com.example.uberdrivermicroservice.controller;


import com.example.uberdrivermicroservice.Exception.NotFoundException;
import com.example.uberdrivermicroservice.dto.ResponseDriverDto;
import com.example.uberdrivermicroservice.dto.request.RequestDriverDto;
import com.example.uberdrivermicroservice.services.DriverService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("driver")
@CrossOrigin(value = "http://localhost:8080", methods = {RequestMethod.GET, RequestMethod.DELETE, RequestMethod.POST, RequestMethod.PATCH, RequestMethod.PUT})
public class DriverController {

    @Autowired
    private DriverService _driverService;


    @PostMapping("")
    public ResponseEntity<ResponseDriverDto> post(@RequestBody RequestDriverDto driverDto) {
        try {
            if (_driverService.findDriverByEmail(driverDto.getEmail()) != null) {
                ResponseDriverDto dto = new ResponseDriverDto();
                dto.setMessage("Passenger already exist");
                return new ResponseEntity<>(dto, HttpStatus.NOT_FOUND);

            }

        } catch (NotFoundException e) {
            return ResponseEntity.ok(_driverService.createDriver(driverDto));
        }
        return null;
    }


    @GetMapping("search/{email}")
    public ResponseEntity<ResponseDriverDto> get(@PathVariable String email) {
        try {
            return ResponseEntity.ok(_driverService.findDriverByEmail(email));
        } catch (NotFoundException e) {
            ResponseDriverDto dto = new ResponseDriverDto();
            dto.setMessage("Driver doesn't exist");
            return new ResponseEntity<>(dto, HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("{id}")
    public ResponseEntity<ResponseDriverDto> get(@PathVariable int id) {
        try {
            return ResponseEntity.ok(_driverService.findDriverById(id));
        } catch (Exception e) {
            ResponseDriverDto dto = new ResponseDriverDto();
            dto.setMessage("Driver doesn't exist");
            return new ResponseEntity<>(dto, HttpStatus.NOT_FOUND);
        }

    }


}
