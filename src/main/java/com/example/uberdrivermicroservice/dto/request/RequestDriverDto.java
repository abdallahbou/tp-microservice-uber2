package com.example.uberdrivermicroservice.dto.request;

import lombok.Data;

@Data
public class RequestDriverDto {

    private String email;

    private String car;

}
