package com.example.uberdrivermicroservice.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class ResponseDriverDto {

    private int id;

    private String email;

    private String car;

    public String message;


}
