package com.example.uberdrivermicroservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class UberDriverMicroserviceApplication {

    public static void main(String[] args) {
        SpringApplication.run(UberDriverMicroserviceApplication.class, args);
    }

}
